
import numpy as np
import pandas as pd


# Ok so we're going to do this by doing one sweep of policy evaluation, and then policy Improvement. 

class rlalgorithm:
    def __init__(self, goalXY, walls, pits, reward_decay=0.9):
        self.threshold = 0.1
        self.reward_decay = reward_decay
        self.V = np.zeros((10, 10))
        self.policy = np.ones((10,10), dtype=int)
        print("policy: " + str(self.policy))

        # Special cells
        self.goal = goalXY
        print ("Goal: " + str(self.goal))
        self.walls = walls
        print ("Walls: " + str(self.walls))
        self.pits = pits
        print ("pits: " + str(self.pits))
        self.states = []
        self.actions = list(range(4))
        self.display_name="Sarsa"


        for i in range(10):
            for j in range(10):
                self.states.append((i,j))
        print ("states: " + str(self.states))

    # This looks good, except that next_state and computeReward must be implemented.
    def policy_evaluation(self):
        while(True):
            delta = 0
            infiniteLoopBreakCondition = False
            for s in self.states:
                if (s in self.walls or s in self.pits or s in self.goal):
                    # print (s)
                    continue
                v = self.V[s]
                action = self.policy[s]
                next_state, reward, done = self.nextStateAndReward(s, action)
                # Might wanna use a probability function instead for policy, which would change this a bit, but tbh this should work
                self.V[s] =  reward + self.reward_decay*self.V[next_state]
                delta = max(delta, np.abs(v - self.V[s]))
                if (self.V[s] < -50):
                    infiniteLoopBreakCondition = True

            # print(self.V)

            if delta < self.threshold or infiniteLoopBreakCondition:
                # print("V: " + str(self.V))
                break

    # def policy(self, s):
    # 	return random.randint(0,3)

    # TODO: might need to swap coordinates for states
    def nextStateAndReward(self, s, action):
        next_state = s
        if (action == 0):
            # print("Action is 0")
            if (s[0] > 0):
                next_state = (s[0]-1, s[1])
        elif(action == 1):
            if (s[0] < 9):
                next_state = (s[0] + 1, s[1])
        elif(action == 2):
            if (s[1] < 9):
                next_state = (s[0], s[1] + 1)
        elif(action == 3):
            if (s[1] > 0):
                next_state = (s[0], s[1] - 1)
        else:
            print("OH NO SOMETHING IS WRONG")


        # Compute reward and also reverse nextstate in special cases
        # Was having some weird float comparison issues so I converted the rewards to integers and then back to floats
        if (next_state in self.goal):

            reward = 1
            done = True
        elif next_state in self.walls:
            reward = -0.3
            done = False
            next_state = s
        elif next_state in self.pits:
            # print('PITS: ' + str(next_state) + "and current state: " + str(s))
            reward = -10
            done = True
        else:
            # print("Reward is -0.1")
            reward = -0.1
            done = False
        return next_state, reward, done

    def policy_improvement(self):
        policy_stable = True
        for s in self.states:
            old_action = self.policy[s]
            self.policy[s] = self.pickHighestValue(s)

            if(old_action != self.policy[s]):
                print("policy is unstable")
                policy_stable = False
            else:
                print("policy is stable")

        # If this is true we are done, somehow we must get the function to stop and V and policy are returned. 
        # If this is false, then the function calling this must deal with that by calling policy iteration again. 
        return policy_stable



    # To do this I'm gonna need some function to calculate next state (which I have), and also some function to calculated the reward, which I don't have
    def pickHighestValue(self, s):
        # DEBUG
        if (s == (3,4)):
            print("HOLY FUCK")
        maxValue = -10000
        selectedAction = 0
        for a in self.actions:
            #maxQ = max(maxQ, self.q_table.loc[s_, a])
            nextState, reward, done = self.nextStateAndReward(s, a)
            temp = reward + self.reward_decay*self.V[nextState]
            if (s == (3,4)):
                print("Action: " + str(a))
                print("temp: " + str(temp))
            if(temp >= maxValue):
                if (s == (3,4)):
                    print("in the if loop: " + str(temp))
                selectedAction = a
                maxValue = temp
        if (s == (3,4)):
            print("Action Selected: " + str(selectedAction))
        return selectedAction

# Assume that one iteration of PE and PI have been done, and so I can just use the policy to pick an action
    def choose_action(self, current_state):
        current_state = (int(current_state[0] // 40), int(current_state[1]//40))
        # print (current_state)
        # TODO: check if this is in the correct format, otherwise I'll need to hack this together so that it looks like qlearning
        action = self.policy[current_state]
        # print ("action: " + str(action))
        return action

    #Do nothing, I'm just leaving this here so I don't have to modify the main function too much 
    def learn(self, s, a, r, s_):
        a_ = self.choose_action(s_)
        return s_, a_


# Lets try debugging by just running it here:

if __name__ == '__main__':
    goalXY = [(4,4)]
    walls = [(2,2),(6,3)]
    pits = [(3,6),(4,1)]
    PI = rlalgorithm(goalXY, walls, pits, reward_decay=1)

    policy_stable = False
    while (not policy_stable):
        PI.policy_evaluation()
        # break
        policy_stable = PI.policy_improvement()

    PI.policy_evaluation()
    print(PI.policy)
    print(PI.V)

    # testPolicy = np.array([[2, 2, 2, 2, 1,3,3,3,3,3]
    #     ,[3,3,3,1,1,1,3,3,3,3]
    #     ,[3,3,2,2,1,3,3,3,3,3]
    #     ,[3,3,2,2,1,3,3,1,1,1]
    #     ,[3,3,2,2,3,3,3,3,3,3]
    #     ,[3,3,3,2,0,3,3,3,3,3]
    #     ,[3,3,3,3,0,0,0,0,0,0]
    #     ,[3,3,3,3,3,3,3,3,3,3]
    #     ,[3,3,3,3,3,3,3,3,3,3]
    #     ,[3,3,3,3,3,3,3,3,3,3]])


    # PI.policy = testPolicy

    # PI.policy_evaluation()
    # PI.V[(4,4)] = 99
    # print(PI.V)
    # PI.policy[(4,4)] = 99
    # print(PI.policy)
    # print(PI.V[(2,4)])

    # print(PI.policy[(3,4)])


# Important given information:


# What we need
	# Initilizize V(s) arbitrarily  (I think the terminal statees might need to be 0, but I can also just make everything 0)
	# pi(s) is initially random

	# Policy evaluation function
		# loop
		# delta
		# 


# what I can do:
	# One round of policy evaluation and then policy improvement
	# Then once I do that we choose action based on the new policy
	# Figure out how to tell if episode is terminated
	# Then we repeat again (until the policy can't be improved anymore)

# First task:
	# Try writing out the code to get policy iteration and evaulation working (abstracting out anything that involves the environment)


    # maybe I should try to change the terminal tasks to 0, as well as the walls... (and the policy for the walls should maybe be something special too?) 

    # Might need to double check specific values of policy_eval, but I think it's fine, it's policy improvement that's broken