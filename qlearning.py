import numpy as np
import pandas as pd


class rlalgorithm:

    def __init__(self, actions, learning_rate=0.01, reward_decay=0.9, e_greedy=0.1):
        self.actions = actions  
        self.lr = learning_rate
        self.gamma = reward_decay
        self.epsilon = e_greedy
        self.q_table = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.display_name="Q learning"

    '''Choose the next action to take given the observed state using an epsilon greedy policy'''
    def choose_action(self, current_state):
        self.check_state_exist(current_state)
 
        #BUG: Epsilon should be .1 and signify the small probability of NOT choosing max action
        # TODO: IS THERE A BUG, I DON'T SEE IT
        if np.random.uniform() >= self.epsilon:
           
           #TODO: Might want to print this out to see exactly what it's doing
            state_action = self.q_table.loc[current_state, :]
           
           # Unsure what the random is here for, or what it does? is this incase index returns more than one index? can it even do that?
            action = np.random.choice(state_action[state_action == np.max(state_action)].index)
        else:
         # We just pick a random action
            action = np.random.choice(self.actions)
        return action


    '''Update the Q(S,A) state-action value table using the latest experience
       This is a not a very good learning update 
    '''
    def learn(self, s, a, r, s_):
        self.check_state_exist(s_)
        if s_ != 'terminal':
            a_ = self.choose_action(str(s_))
            q_target = r + self.gamma * self.pickHighestQ(s_)
        else:
            q_target = r  # next state is terminal
        self.q_table.loc[s, a] += self.lr*(q_target - self.q_table.loc[s, a])  # update
        return s_, a_


    '''States are dynamically added to the Q(S,A) table as they are encountered'''
    def check_state_exist(self, state):
        if state not in self.q_table.index:
            # append new state to q table
            self.q_table = self.q_table.append(
                pd.Series(
                    [0]*len(self.actions),
                    index=self.q_table.columns,
                    name=state,
                )
            )


    def pickHighestQ(self, s_):
    	maxQ = 0
    	for a in self.actions:
    		maxQ = max(maxQ, self.q_table.loc[s_, a])

    	return maxQ