
import numpy as np
import pandas as pd


class rlalgorithm:
   
    def __init__(self, actions, learning_rate=0.01, reward_decay=0.9, e_greedy=0.1):
        self.actions = actions  
        self.lr = learning_rate
        self.gamma = reward_decay
        self.epsilon = e_greedy
        self.q_table = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.display_name="value iteration"

    def choose_action(self, observation):
	# implement this. 
		state_action = self.q_table[observation, :]
        return action

    def learn(...):
	#implement this. Learn something from the transition

	#What we need
		# A threshold
		# Initial values for very V(s)
		